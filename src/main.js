// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import {
    Radio,
    Alert,
    Swiper,
    SwiperItem,
    Marquee,
    MarqueeItem,
    PopupPicker,
    Cell,
    Group,         
    Datetime,
    Toast,
    XTextarea,
    Selector,
    ToastPlugin,
    LoadingPlugin,
    AlertPlugin,
    Previewer,
    ConfirmPlugin
} from 'vux'
import preview from 'vue-photo-preview'
import 'vue-photo-preview/dist/skin.css'
import Uploader from 'vux-uploader'
import '@static/css/reset.css'
import '@static/css/formStyle.css'
import '@static/fontSize/iconfont.css'
import $axiosAjax from './axios'
import Vuex from 'vuex'
import store from './vuex/store.js'
import ElementUi from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import VueJsBridge from 'vue-webview-js-bridge'
import VueTouch from 'vue-touch'
Vue.use(VueTouch, {name: 'v-touch'})
Vue.prototype.$axiosAjax = $axiosAjax



Vue.config.productionTip = false
Vue.use(VueJsBridge, {
    debug: true,
    delay: 200,
    nativeHandlerName: 'callNativeHandler',
    mock: false,
    mockHandler: null
})
Vue.use(preview)
Vue.use(ElementUi)
Vue.use(ToastPlugin)
Vue.use(LoadingPlugin)
Vue.use(AlertPlugin)
Vue.use(ConfirmPlugin)
Vue.use(Vuex)
Vue.component('Previewer',Previewer)
Vue.component('alert', Alert)
Vue.component('Uploader', Uploader)
Vue.component('swiper', Swiper)
Vue.component('swiper-item', SwiperItem)
Vue.component('marquee', Marquee)
Vue.component('marquee-item', MarqueeItem)
Vue.component('popup-picker', PopupPicker)
Vue.component('cell', Cell)
Vue.component('group', Group)
Vue.component('datetime', Datetime)
Vue.component('toast', Toast)
Vue.component('x-textarea', XTextarea)
Vue.component('selector', Selector)
Vue.component('radio', Radio)
/*router.beforeEach((to, from, next) => {
    if(from.fullPath!="/hideRegister"){
        window.scrollTo(0, 0)
    }
    if (to.meta.title) {
        document.title = to.meta.title
    }
    next()
})*/

VueTouch.config.swipe = {

  threshold: 100 //手指左右滑动距离

}
Vue.filter('toChinesNum', (num) => {
    let changeNum = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
    let unit = ['', '十', '百', '千', '万']
    num = parseInt(num)
    let getWan = (tem) => {
        let strArr = tem.toString().split('').reverse()
        let newNum = ''
        for (let i = 0; i < strArr.length; i++) {
            newNum = (i == 0 && strArr[i] == 0 ? '' : (i > 0 && strArr[i] == 0 && strArr[i - 1] == 0 ? '' : changeNum[strArr[i]] + (strArr[i] == 0 ? unit[0] : unit[i]))) + newNum
        }
        return newNum
    }
    let overWan = Math.floor(num / 10000)
    let noWan = num % 10000
    if (noWan.toString().length < 4) {
        noWan = `0${noWan}`
    }
    return overWan ? `${getWan(overWan)}万${getWan(noWan)}` : getWan(num)
})

Vue.filter('myTrim', (value) => {
    let str = value ? value : ''
    return str.replace(/(^\s*)|(\s*$)/g, '')
})
/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components: {
        App
    },
    template: '<App/>'
})
