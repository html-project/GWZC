// 打包生产/测试环境
export const IsDevelopment = process.env.VUE_RUN_TYPE === 'test'
// 是否是本地启动
export const IsLocalHost= process.env.NODE_PROJECT === 'localhost'

let defaultURL
if (IsLocalHost) {
    // 本地联调时URL配置地址(可更改，仅限本地调试)
    //  defaultURL = 'http://zcsms-sit.bosafe.com'
     defaultURL = 'http://localhost:21021/'
} else {
    // 打包测试/生产环境（勿动）
    defaultURL= IsDevelopment ? 'http://zcweb.bosafe.com' : 'http://zcweb.bosafe.com'
}
// 请求baseURL
export const DefaultURL = defaultURL

console.log('打包的baseURL')
console.log(DefaultURL)