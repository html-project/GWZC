import http from './axiosAjax'
import { DefaultURL } from '@/utils/const'

export default {
  getConfigJson: (url, params, formData) => { // 示例
    if (formData) {
      return http.fetchGet(url, params, formData)
    } else {
      return http.fetchGet(url, params)
    }
  },
  postConfigJson: (url, params, formData) => { // 示例
    if (formData) {
      return http.fetchPost(url, params, formData)
    } else {
      return http.fetchPost(url, params)
    }
  },
  getAssessmentPlan: (params) => { // 量化考核计划
    return http.fetchPost('/api/services/app/Assessment/GetPageList', params)
  },
  AppGetPageListAll: (params) => { // 待审核量化考核计划列表
    return http.fetchPost('/api/services/app/Assessment/AppGetPageListAll', params)
  },
  login: (params) => { // 登录
    return http.fetchLogin('/api/TokenAuth/Authenticate', params)
  },
  GetPageListAssessment: (params) => {
    return http.fetchPost('/api/services/app/Assessment/GetRecordPageList', params)
  },
  getTableTypeList: (params) => { // 考核类型
    return http.fetchPost('/api/services/app/QuantizeAssessment/tableTypeList', params)
  },
  examinedUnits: (params) => { // 被考核单位
    return http.fetchLogin('/api/services/app/Depart/GetAllDepartList', params)
  },
  getAssessmentPlanInfo: (params) => { // 获取量化考核记录信息
    return http.fetchPost('api/services/app/Assessment/GetQuantizeCheckRecord', params)
  },
  getUserList: (params) => { // 考核组长
    return http.fetchPost('/api/services/app/SysUser/GetList', params)
  },
  getDivisionList: (params) => { // 分工考核
    return http.fetchPost('/api/services/app/Division/GetDivisionList', params)
  },
  getPalnInfo: (params) => { // 获取量化考核计划信息
    return http.fetchPost('/api/services/app/Assessment/Get', params)
  },
  ClassAssessmentTotal: (params) => { // 考核计划统计
    return http.fetchPost('/api/services/app/Assessment/ClassAssessmentTotal', params)
  },
  save: (params) => { // 新增考核计划
    return http.fetchPost('/api/services/app/Assessment/Create', params)
  },
  approversubmit: (params) => { // 审核考核计划
    return http.fetchPost('/api/services/app/Assessment/ApproverSubmit', params)
  },
  getRecordInfo: (params) => { // 考核计划详情
    return http.fetchPost('/api/services/app/Assessment/GetQuantizeCheckRecord', params)
  },
  getQuantizeAssessment: (params) => { // 列表 量化考核--合理缺项树
    return http.fetchPost('/api/services/app/QuantizeAssessment/GetList', params)
  },
  getQuantizeStandardList: (params) => {
    return http.fetchPost('api/services/app/QuantizeStandard/GetList', params)
  },
  GetRecordMissingItem: (params) => {   //  获取量化考核记录合理缺项
    return http.fetchPost('/api/services/app/Assessment/GetH5RecordMissingItem', params)
  },
  GetH5RecordQuestion: (params) => {   //  获取量化考核记录问题项
    return http.fetchPost('/api/services/app/Assessment/GetH5RecordQuestion', params)
  },
  haveRegBreakList: (params) => {
    return http.fetchPost('/api/services/app/Weizregister/GetAll', params)
  },
  haveRegBreakGet: (params) => {
    return http.fetchPost('/api/services/app/Weizregister/Get', params)
  },

  saveWeizregister: (params) => { // 登记违章创建违章
    return http.fetchPost('/api/services/app/Weizregister/Create', params)
  },
  updateWeizregister: (params) => { // 登记违章修改违章
    return http.fetchPost('/api/services/app/Weizregister/Update', params)
  },
  getweizregisterInfo: (params) => { // 获取违章登记信息
    return http.fetchPost('/api/services/app/Weizregister/Get', params)
  },
  uploadImg: (formData) => { // 上传违章图片
    return http.fetchUpload('/api/services/app/Weizregister/UploadFileWeiregister', formData)
  },
  weizPhenoList: (params) => { // 违章现象
    return http.fetchPost('/api/services/app/WeizLibrary/GetList', params)
  },
  getQuantizeClassifyStandardList: (params) => { // 获取类别子类及评分项
    return http.fetchPost('/api/services/app/QuantizeStandard/GetQuantizeClassifyStandardList', params)
  },
  getStandardListByClassifyId: (params) => { // 获取类别下的评分项
    return http.fetchPost('/api/services/app/QuantizeStandard/GetStandardListByClassifyId', params)
  },
  qusetionInfo: (params) => { // 整改通知书上面的信息
    return http.fetchPost('/api/services/app/Qusetion/Get', params)
  },
  AppGetRectificationData: (params) => { // APP整改通知书上面的信息
    return http.fetchPost('/api/services/app/Qusetion/AppGetRectificationData', params)
  },
  GetRecordQuestion: (params) => { // 获取量化考核的问题描述
    return http.fetchPost('/api/services/app/Qusetion/GetRecordQuestion', params)
  },
  createRectification: (params) => { // 移动端新增整改通知单
    return http.fetchPost('/api/services/app/Qusetion/CreateRectification', params)
  },
  AppCreateRectification: (params) => { // 移动端新增整改通知单
    return http.fetchPost('/api/services/app/Qusetion/AppCreateRectification', params)
  },
  getQuantizeStandardTreeList: (params) => { // 获取评分项
    return http.fetchPost('/api/services/app/QuantizeStandard/GetQuantizeStandardList', params)
  },
  uploadFile: (params) => {// 上传文件
    return http.fetchPost('/api/service/app/File/Upload', params)
  },
  createQuantizeCheckRecord: (params) => { // 创建量化考核记录
    return http.fetchPost('/api/services/app/Assessment/CreateH5QuantizeCheckRecord', params)
  },
  updateQuantizeCheckRecord: (params) => { // 修改量化考核记录
    return http.fetchPost('/api/services/app/Assessment/UpdateH5QuantizeCheckRecord', params)
  },
  getRecordClassifyTree: (params) => {
    return http.fetchPost('/api/services/app/Assessment/GetRecordClassifyTree', params);
  },
  checkNotice: (params) => { // 积分通知单列表
    return http.fetchPost('/api/services/app/WeizregisterSend/GetAll', params)
  },
  getNotice: (params) => { // 积分通知单详情
    return http.fetchPost('/api/services/app/WeizregisterSend/Get', params)
  },
  createNotice: (params) => { // 下发积分通知单
    return http.fetchPost('/api/services/app/WeizregisterSend/Create', params)
  },
  auditRecord: (params) => { // 审核考核记录
    return http.fetchPost('/api/services/app/Assessment/AuditRecord', params);
  },
  checkRecord: (params) => { // 考核考核记录
    return http.fetchPost('/api/services/app/Assessment/CheckRecord', params);
  },
  getCurrentUserRoleList: (params) => { // 获取当前用户角色列表
    return http.fetchPost('/api/services/app/SysUser/GetCurrentUserRoleList', params);
  },
  setCurrentUserRole: (params) => { // 设置当前用户角色
    return http.fetchPost('/api/services/app/SysUser/SetCurrentUserRole', params);
  },
  getUserInfo: (params) => { // 获取当前用户信息
    return http.fetchPost('/api/services/app/SysUser/GetUserInfo', params);
  },
  getBuildRelationProject: (params) => { // 获取参建单位关联的项目部
    return http.fetchPost('/api/services/app/Depart/GetBuildRelationProject', params);
  },
  getBuildRelationProjectWZ: (params) => { // 获取参建单位关联的项目部
    return http.fetchPost('/api/services/app/Depart/GetDepartInitialDataWZ', params);
  },
  getDivision: (params) => { // 分工
    return http.fetchPost('/api/services/app/Assessment/GetDivisionClassity', params)
  },
  upload:(params)=>{
    return http.fetchUpload('/api/services/app/Upload/UploadFile',params);
  },
  assessmentUpdate: (params) => { // 考核计划修改
    return http.fetchPost('/api/services/app/Assessment/Update', params)
  },
  GetQuestionClassity: (params) => { // 考核计划修改
    return http.fetchPost('/api/services/app/Qusetion/GetQuestionClassity', params)
  },
  getWzTree: (params) => {
    return http.fetchUpload('/api/services/app/WeizLibrary/GetList',params);
  },
  // 违章整改
  illegalGetAll: (params) => {
    return http.fetchPost('/api/services/app/Weizregister/GetAllWZmobile',params);
  },
  // 违章整改 --详情
  illegalGet: (params) => {
    return http.fetchPost('/api/services/app/Weizregister/Get',params);
  },
  // 提交 违章整改  --我的
  illegalSubmit: (params) => {
    return http.fetchPost('/api/services/app/Weizregister/Update',params);
  },
  // 提交 违章整改  --图片
  illegalUpload:(params)=>{
    return http.fetchUpload('/api/services/app/Weizregister/UploadFilebyMoible',params);
  },
  // 删除 违章整改  --图片
  illegalDelete:(params)=>{
    return http.fetchPost('/api/services/app/Depart/DeleteFile',params);
  },
  getAllWZmobile: (params) => { // 违章查询
    return http.fetchPost('/api/services/app/Weizregister/GetAllWZmobile', params)
  },
  getBacklogList: (params) => { // 获取代办
    return http.fetchPost('/api/services/app/Qusetion/GetBacklogListApp', params)
  },
  rectificationPro: (params) => { // 获取代办详情
    return http.fetchPost('/api/services/app/RectificationPro/Get', params)
  },
  rectificationProSave: (params) => { // 获取代办详情
    return http.fetchPost('/api/services/app/RectificationPro/Save', params)
  },
  // 删除 违章整改  --图片
  phenomenonList:(params)=>{
    return http.fetchPost('/api/services/app/WeizPheno/GetList',params);
  },
  rectificationList: (params) => { // 获取整改问题列表
    return http.fetchPost('/api/services/app/Qusetion/GetRectificationList', params)
  },
  rectificationDetail: (params) => { // 获取整改详情
    return http.fetchPost('/api/services/app/Qusetion/GetRectificationData', params)
  },
  queryTypeBynodeId: (params) => { // 问题描述树
    return http.fetchPost('/api/services/app/Qusetion/GetQusetionItems', params)
  },
  H5GetQusetionItems: (params) => { // 问题描述树
    return http.fetchPost('/api/services/app/Qusetion/H5GetQusetionItems', params)
  },
  getAdress: (params) => { // 地址
    return http.fetchPost('/api/services/app/Qusetion/getCheckAdressList', params)
  },
  getCheckTypeList: (params) => { // 公共接口根据编码名称获取集合(查询条件-编码类别名称codeName 必填)
    return http.fetchPost('/api/services/app/SafeCheck/getCodeNameList', params)
  },
  querycheckPlanAssess: (params) => {
    return http.fetchPost('/api/services/app/SafeCheck/GetPageList', params)
  },
  getcheckPlanAssess: (params) => {
    return http.fetchPost('/api/services/app/SafeCheck/Get', params)
  },
  savecheckPlanAssess: (params) => {
    return http.fetchPost('/api/services/app/SafeCheck/Approval', params)
  },
/*  getIndex: (params) => { // 地址
    return http.fetchPost('/api/services/app/SafeJc/GetIndex', params)
  },*/
  getIndex: (params) => { // 地址
    return http.fetchPost('/api/services/app/SafeJc/GetIndexList', params)
  },
  creatPlan: (params) => { // 新增接口
    return http.fetchPost('/api/services/app/SafeCheck/CreateAsync', params)
  },
  getInfoList: (params) => { // 声成通知单
    return http.fetchPost('/api/services/app/SafeCheck/GetHiddenDangerInfoList', params)
  },
  getCheckList: (params) => { // 安全检查台账
    return http.fetchPost('/api/services/app/SafeCheck/GetPageList', params)
  },
  getNeedCheckList: (params) => { // 需要执行的检查
    return http.fetchPost('/api/services/app/SafeCheck/NeedImplementCheck', params)
  },
  getSafeDetails: (params) => { // 需要执行的检查详情
    return http.fetchPost('/api/services/app/SafeCheck/Get', params)
  },
  safeCheckResult: (params) => { // 提交需要执行的安全检查
    return http.fetchPost('/api/services/app/SafeCheck/SafeCheckResult', params)
  },
  gethiddenNoticeList: (params) => { // 通知单勾选列表
    return http.fetchPost('/api/services/app/SafeCheck/GetHiddenDangerInfoList', params)
  },
  getHiddenPageList: (params) => { // 隐患台账列表查询
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetPageList', params)
  },
  getHiddenPageList_DB: (params) => { // 隐患台账列表查询
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetPageList_DB', params)
  },
  getHiddenInfo: (params) => { // 隐患查询
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Get', params)
  },
  deleteHiddenDangerInfo: (params) => { // 隐患删除
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Delete', params)
  },
  saveHiddenDangerInfo: (params) => { // 隐患保存(登记/整改/验收)
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Save', params)
  },
  submitHiddenDangerInfo: (params) => { // 隐患提交(登记/整改/验收)
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Submit', params)
  },
  submitHiddenDangerInfoAPP: (params) => { // 隐患提交(登记/整改/验收)app端
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Submit_App', params)
  },
  getHiddenDangerType: (params) => { // 获取隐患类别
    return http.fetchPost('/api/services/app/HiddenDangerInfo/getHiddenDangerType', params)
  },
  getHiddenDangerProject: (params) => { // 获取项目类型
    return http.fetchPost('/api/services/app/Checkstandard/getHiddenDangerType', params)
  },
  getHiddenDangerStatus: (params) => { // 获取隐患状态
    return http.fetchPost('/api/services/app/HiddenDangerInfo/getHiddenDangerStatus', params)
  },
  getHiddenDangerLevel: (params) => { // 获取隐患级别
    return http.fetchPost('/api/services/app/HiddenDangerInfo/getHiddenDangerLevel', params)
  },
  getHiddenDangerStatus2: (params) => { // 获取隐患类别
    return http.fetchPost('/api/services/app/Checkstandard/getHiddenDangerStatus', params)
  },
  getStandard: (params) => { // 获取隐患描述
    return http.fetchPost('/api/services/app/Checkstandard/GetList', params)
  },
  getDepartListzg: (params) => { // 整改单位 执行人,班组,负责人
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetDepartListzg', params)
  },
  getDelete_H5: (params) => { // 整改单位 执行人,班组,负责人
    return http.fetchPost('/api/services/app/HiddenDangerInfo/Delete_H5', params)
  },
  getLevelStatistics: (params) => { // 隐患统计（未选择统计指标，默认）
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetLevelStatistics', params)
  },
  GetRectifyRateStatistics: (params) => { // 隐患整改率统计
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetRectifyRateStatistics', params)
  },
  GetOntRectifyRateStatistics: (params) => { // 一次性隐患整改率统计
    return http.fetchPost('/api/services/app/HiddenDangerInfo/GetOntRectifyRateStatistics', params)
  },
  getH5Statistics: (params) => { // H5隐患统计
    return http.fetchPost('/api/services/app/HiddenDangerInfo/H5GetStatistics', params)
  },
  deletePlanbyId: (params) => { // 删除量化考核计划
    return http.fetchPost('/api/services/app/Assessment/Delete', params)
  },
  // baseurl: 'http://zcsms-sit.bosafe.com/'
  
  GetBuildingProject: (params) => { // 获取在线项目
    return http.fetchPost('/api/services/app/HomePage/GetBuildingProject', params)
  },
  GetHiddenDangerIndex: (params) => { // 首页隐患指标
    return http.fetchPost('/api/services/app/HomePage/GetHiddenDangerIndex', params)
  },
  GetProjectZgRate: (params) => { // 获取各个项目隐患整改率
    return http.fetchPost('/api/services/app/HomePage/GetProjectZgRate', params)
  },
  GetProjectIndex1: (params) => { // 首页指标1
    return http.fetchPost('/api/services/app/HomePage/GetProjectIndex?type=1', params)
  },
  GetProjectIndex2: (params) => { // 首页指标2
    return http.fetchPost('/api/services/app/HomePage/GetProjectIndex?type=2', params)
  },
  GetProjectIndex: (params) => { // 首页指标2
    return http.fetchPost('/api/services/app/HomePage/GetProjectIndex', params)
  },
  GetMobileWZ:(params) => { // 违章统计
    return http.fetchPost('/api/services/app/Weizregister/GetMobileWZ', params)
  },
  baseurl: DefaultURL
}
