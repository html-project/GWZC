import Axios from 'axios'

// 设置baseUrl
Axios.defaults.baseURL = ''

Axios.interceptors.request.use(
  config => {
    // let token = localStorage.token
    // // 设置token值
    // config.headers.token = token
    // // 设置请求头
    config.headers['Content-type'] = 'application/json'
//     if (config.method === 'post') {
// //       config.data.tokenId = 'A3944C2D655B11D684296E71CB81C9BD'
// //       config.data.userId = '615123137'
//       let formData = new FormData()
//       formData.append('data', JSON.stringify(config.data))
//       config.data = formData
//     }
    return config
  },
  error => {
    return error
  })

Axios.interceptors.response.use(
  res => {
    return res
  },
  error => {
    return error
  })

export default async (url = '', data = {}, type = 'GET', method = 'fetch') => {
  if (type === 'GET') {
    return new Promise((resolve, reject) => {
      // get方法加个时间参数，解决ie可能缓存问题
      data.t = new Date().getTime()
      Axios.get(url, {
        params: data
      })
        .then(response => {
          resolve(response.data)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
  if (type === 'POST') {
    return new Promise((resolve, reject) => {
      data.business = url
      Axios.post('/nzcgjsms/api/AppService.ashx', data)
        .then(response => {
          resolve(response.data)
        })
        .catch(err => {
          reject(err)
        })
    })
  }
}
