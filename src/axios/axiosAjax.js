import axios from 'axios'
import qs from "qs"
import store from '../vuex/store.js'
import { DefaultURL } from '@/utils/const'
axios.defaults.timeout = 10000
axios.defaults.headers['Content-Type'] = 'application/json'
//
//  if(store.state.Authorization){
// 	axios.defaults.withCredentials = true;
// }else{
// 	axios.defaults.withCredentials = false;
// }
// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
// axios.defaults.headers['Content-Type'] = 'multipart/form-data'
axios.defaults.baseURL = DefaultURL
axios.defaults.headers['Authorization']= window.localStorage.getItem('token')
axios.interceptors.request.use(
  config => {
    if (config.method === 'post') {
      if(store.state.Authorization){
        config.withCredentials = true
        config.headers.Authorization = store.state.Authorization
        // config.headers.Cookie.Abp.AuthToken = store.state.Authorization
        //config.headers.Cookie =  'Abp.AuthToken='+store.state.Authorization;
      }
    }
    return config
  },
  error => {
    return error
  })
axios.interceptors.response.use(
  res => {
    return res
  },
  error => {
    return error
  })
export default {
  fetchGet (url, params = {}) {
    return axios.get(url, params).then((res) => {
      return Promise.resolve(res)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  fetchPost (url, params = {}, formData = new FormData()) {
    // formData.append('data', )
    return axios({
      url: url,
      method: 'POST',
      data: JSON.stringify(params)
    }).then((res) => {
      return Promise.resolve(res)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  fetchLogin(url, params = {}, formData = new FormData()) {
    return axios({
      url: url,
      method: 'POST',
      data: JSON.stringify(params)
    }).then((res) => {
      return Promise.resolve(res)
    }).catch((error) => {
      return Promise.reject(error)
    })
  },
  fetchUpload(url,formData){
    let config={
      headers:{"Content-Type":"multipart/form-data"}
    };
    return axios.post(url,formData,config).then((res)=>{
      return Promise.resolve(res);
    }).catch((error)=>{
      return Promise.reject(error)
    })
  }
}
