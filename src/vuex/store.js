
import actions from "./action.js";
import state from "./state.js";
import mutations from "./mutations.js";

import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";

Vue.use(VueRouter);
Vue.use(Vuex);
var store = new Vuex.Store({
    state,
    actions,
    mutations
});
export default store;
