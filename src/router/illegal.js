export default[
    {
        path: '/illegal', // 违章整改
        name: 'illegal',
        component: (resolve) => require(['@/components/illegal/illegal'], resolve),
        meta:{
            title:'违章整改'
        }
    },
    {
        path: '/illegalDetails', // 违章整改--全部
        name: 'illegalDetails',
        component: (resolve) => require(['@/components/illegal/illegalDetails'], resolve),
        meta:{
            title:'违章整改详情'
        }
    },
    {
        path: '/illegalDetails2', // 违章整改--我的
        name: 'illegalDetails2',
        component: (resolve) => require(['@/components/illegal/illegalDetails2'], resolve),
        meta:{
            title:'违章整改详情'
        }
    },
    {
        path: '/illegalStatistics', // 违章统计
        name: 'illegalStatistics',
        component: (resolve) => require(['@/components/illegal/illegalStatistics'], resolve),
        meta:{
            title:'违章整改详情'
        }
    },
]