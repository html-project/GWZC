export default[
    {
        path: '/register', // 登记违章
        name: 'register',
        component: (resolve) => require(['@/components/register/register'], resolve),
        meta:{
            title:'登记违章'
        }
    },
    {
        path: '/register2', // 登记违章
        name: 'register2',
        component: (resolve) => require(['@/components/register/register2'], resolve),
        meta:{
            title:'登记违章'
        }
    },
    {
        path: '/register3', // 登记违章
        name: 'register3',
        component: (resolve) => require(['@/components/register/register3'], resolve),
        meta:{
            title:'登记违章'
        }
    },
    {
        path: '/phenomenon', // 违章整改
        name: 'phenomenon',
        component: (resolve) => require(['@/components/register/phenomenon'], resolve),
        meta:{
            title:'登记违章'
        }
    },
]