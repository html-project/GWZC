export default[
  {
    path: '/quanPlan', // 量化考核计划
    name: 'quanPlan',
    component: (resolve) => require(['@/components/quantization/quanPlan'], resolve),
    meta:{
      title:'量化考核计划'
    }
  },
  {
    path: '/addPlan', // 新增量化考核计划
    name: 'addPlan',
    component: (resolve) => require(['@/components/quantization/addPlan'], resolve),
    meta:{
      title:'新增量化考核计划'
    }
  },
  {
    path: '/planDetails', // 考核计划详情
    name: 'planDetails',
    component: (resolve) => require(['@/components/quantization/planDetails'], resolve),
    meta:{
      title:'考核计划详情'
    }
  },
]
