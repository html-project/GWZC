export default[
  {
    path: '/problemRectify', // 问题项整改
    name: 'problemRectify',
    component: (resolve) => require(['@/components/problemRectify/index'], resolve)
  }, {
    path: '/problemRectifyDetail', // 问题项整改
    name: 'problemRectifyDetail',
    component: (resolve) => require(['@/components/problemRectify/myDetail'], resolve)
  },{
    path: '/problemCheck', // 问题验收
    name: 'problemCheck',
    component: (resolve) => require(['@/components/problemCheck/index'], resolve)
  }, {
    path: '/problemCheckDetail', // 问题验收
    name: 'problemCheckDetail',
    component: (resolve) => require(['@/components/problemCheck/myDetail'], resolve)
  }
]
