export default[
  {
    path: '/changeAbarbeitung', // 整改通知书
    name: 'changeAbarbeitung',
    component: (resolve) => require(['@/components/quantization/changeAbarbeitung'], resolve),
    meta:{
      title:'整改通知书'
    }
  },{
    path: '/breakNotice', // 违章积分通知书
    name: 'breakNotice',
    component: (resolve) => require(['@/components/quantization/breakNotice'], resolve),
    meta:{
      title:'违章记分通知书'
    }
  },
  {
    path: '/safetyInspection', // 安全稽查列表
    name: 'safetyInspection',
    component: (resolve) => require(['@/components/quantization/safetyInspection'], resolve),
    meta:{
      title:'安全稽查列表'
    }
  },
  {
    path: '/securityCheck', // 安全检查列表
    name: 'securityCheck',
    component: (resolve) => require(['@/components/quantization/securityCheck'], resolve),
    meta:{
      title:'安全检查列表'
    }
  },
  {
    path: '/brankAbarbeitung', // 违章整改通知书下发
    name: 'brankAbarbeitung',
    component: (resolve) => require(['@/components/quantization/brankAbarbeitung'], resolve),
    meta:{
      title:'违章整改通知书下发'
    }
  },
  {
    path: '/scoreNoticeDetails',
    name: 'scoreNoticeDetails',
    component: (resolve) => require(['@/components/quantization/scoreNoticeDetails'],resolve),
    meta:{
      title:'违章记分通知书详情'
    }
  },
  {
    path: '/rectificationNotice',
    name: 'rectificationNotice',
    component: (resolve) => require(['@/components/quantization/rectificationNotice'],resolve),
    meta:{
      title:'整改通知单列表'
    }
  },
  {
    path: '/rectificationNotice1',
    name: 'rectificationNotice1',
    component: (resolve) => require(['@/components/quantization/rectificationNotice1'],resolve),
    meta:{
      title:'整改通知单列表'
    }
  },
  {
    path: '/changeAbarbeitungDetails',
    name: 'changeAbarbeitungDetails',
    component: (resolve) => require(['@/components/quantization/changeAbarbeitungDetails'],resolve),
    meta:{
      title:'整改通知单详情'
    }
  },
]
