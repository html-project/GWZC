export default[
    {
      path: '/registerBreakList', // 登记违章列表
      name: 'registerBreakList',
      component: (resolve) => require(['@/components/quantization/registerBreakList'], resolve),
      meta:{
        title:'登记违章列表'
      }
    },
    {
      path: '/registerBreak', // 登记违章录入
      name: 'registerBreak',
      component: (resolve) => require(['@/components/quantization/registerBreak'], resolve),
      meta:{
        title:'登记违章录入'
      }
    },
    {
      path: '/registerBreakWeiTree', // 登记违章录入
      name: 'registerBreakWeiTree',
      component: (resolve) => require(['@/components/quantization/registerBreakWeiTree'], resolve),
      meta:{
        title:'登记违章录入'
      }
    }
  ]
  