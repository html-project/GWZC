export default[
  {
    path: '/hiddenInvestigation', // 问题排查
    name: 'hiddenInvestigation',
    component: (resolve) => require(['@/components/hidden/hiddenInvestigation'], resolve),
    meta:{
      title:'问题排查'
    }
  },
  {
    path: '/quanHidden', // 问题列表查询
    name: 'quanHidden',
    component: (resolve) => require(['@/components/hidden/quanHidden'], resolve),
    meta:{
      title:'问题列表查询',
      keepAlive: true
    }
  },
  {
    path: '/hiddeDetails', // 问题列表查询
    name: 'hiddeDetails',
    component: (resolve) => require(['@/components/hidden/hiddeDetails'], resolve),
    meta:{
      title:'问题详情'
    }
  },
  {
    path: '/hiddenCheck', // 问题排查
    name: 'hiddenCheck',
    component: (resolve) => require(['@/components/hidden/hiddenCheck'], resolve),
    meta:{
      title:'问题排查'
    }
  },
  {
    path: '/hiddenZg', // 问题整改列表
    name: 'hiddenZg',
    component: (resolve) => require(['@/components/hidden/hiddenZg'], resolve),
    meta:{
      title:'问题整改列表'
    }
  },
  {
    path: '/addHiddenZg', // 问题整改
    name: 'addHiddenZg',
    component: (resolve) => require(['@/components/hidden/addHiddenZg'], resolve),
    meta:{
      title:'问题整改'
    }
  },
  {
    path: '/addHiddenYs', // 问题验收
    name: 'addHiddenYs',
    component: (resolve) => require(['@/components/hidden/addHiddenYs'], resolve),
    meta:{
      title:'问题验收'
    }
  },
  {
    path: '/statisticsHidden', // 问题统计
    name: 'statisticsHidden',
    component: (resolve) => require(['@/components/hidden/statisticsHidden'], resolve),
    meta:{
      title:'问题统计'
    }
  },
]
