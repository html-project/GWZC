export default[
   {
        path: '/breakQuery', // 违章查询
        name: 'breakQuery',
        component: (resolve) => require(['@/components/breakQuery/index'], resolve)
      },{
        path: '/breakQueryDetail', // 违章详情
        name: 'breakQueryDetail',
        component: (resolve) => require(['@/components/breakQuery/details'], resolve)
      },{
        path: '/breakQueryTol', // 违章统计
        name: 'breakQueryTol',
        component: (resolve) => require(['@/components/breakQuery/indextol'], resolve)
      },
]
