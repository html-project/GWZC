export default[

  {
    path: '/safeAccount', // 安全检查台账
    name: 'safeAccount',
    component: (resolve) => require(['@/components/safeCheck/safeAccount'], resolve)
  },
  {
    path: '/safeDetails', // 安全检查台账详情
    name: 'safeDetails',
    component: (resolve) => require(['@/components/safeCheck/safeDetails'], resolve)
  },
  {
    path: '/checkPlanAssess', // 检查计划审批
    name: 'checkPlanAssess',
    component: (resolve) => require(['@/components/safeCheck/checkPlanAssess'], resolve)
  },
  {
    path: '/assess', // 审批
    name: 'assess',
    component: (resolve) => require(['@/components/safeCheck/assess'], resolve)
  },
  {
    path: '/needCheck', // 需要执行的检查
    name: 'needCheck',
    component: (resolve) => require(['@/components/safeCheck/needCheck'], resolve)
  },
  {
    path: '/leaderCheck', // 执行检查（组长）
    name: 'leaderCheck',
    component: (resolve) => require(['@/components/safeCheck/leaderCheck'], resolve)
  },
  {
    path: '/memberCheck', // 执行检查（组员）
    name: 'memberCheck',
    component: (resolve) => require(['@/components/safeCheck/memberCheck'], resolve)
  },
  {
    path: '/registerDanger', // 问题登记
    name: 'registerDanger',
    component: (resolve) => require(['@/components/safeCheck/registerDanger'], resolve)
  },
  {
    path: '/registerDangerNew', // 问题登记
    name: 'registerDangerNew',
    component: (resolve) => require(['@/components/safeCheck/registerDangerNew'], resolve)
  },
  {
    path: '/registerDanger2', // 问题登记
    name: 'registerDanger2',
    component: (resolve) => require(['@/components/safeCheck/registerDanger2'], resolve)
  },
  {
    path: '/needcheckregisterDanger', // 需要执行的安全检查问题登记
    name: 'needcheckregisterDanger',
    component: (resolve) => require(['@/components/safeCheck/needcheckregisterDanger'], resolve)
  },
  {
    path: '/safeCheckIndex', // 安全检查
    name: 'safeCheckIndex',
    component: (resolve) => require(['@/components/safeCheck/safeCheckIndex'], resolve)
  },
  {
    path: '/addCheckRecord3', // 新增检查记录输入框
    name: 'addCheckRecord3',
    component: (resolve) => require(['@/components/safeCheck/addCheckRecord3'], resolve)
  },
  {
    path: '/addCheckPlan', // 新增检查计划
    name: 'addCheckPlan',
    component: (resolve) => require(['@/components/safeCheck/addCheckPlan'], resolve)
  },
  {
    path: '/addCheckRecord1', // 新增检查记录
    name: 'addCheckRecord1',
    component: (resolve) => require(['@/components/safeCheck/addCheckRecord1'], resolve)
  },
  {
    path: '/addCheckRecord2', // 新增检查记录-关联检查项目
    name: 'addCheckRecord2',
    component: (resolve) => require(['@/components/safeCheck/addCheckRecord2'], resolve)
  },
  {
    path: '/checkPlanAssessEdit', // 新增检查记录-关联检查项目
    name: 'checkPlanAssessEdit',
    component: (resolve) => require(['@/components/safeCheck/checkPlanAssessEdit'], resolve)
  },
  {
    path: '/needSafeDetails', // 需要执行的安全检查详情
    name: 'needSafeDetails',
    component: (resolve) => require(['@/components/safeCheck/needSafeDetails'], resolve)
  },
  {
    path: '/safeNotice', // 通知单
    name: 'safeNotice',
    component: (resolve) => require(['@/components/safeCheck/safeNotice'], resolve)
  }
  ]