export default[

    {
      path: '/quanHiddenTotal', // 隐患总数量
      name: 'quanHiddenTotal',
      component: (resolve) => require(['@/components/safeCode/quanHiddenTotal'], resolve)
    },
    {
      path: '/rectHiddenTotal', // 待整改隐患数量
      name: 'rectHiddenTotal',
      component: (resolve) => require(['@/components/safeCode/rectHiddenTotal'], resolve)
    },
    {
      path: '/overdueHiddenTotal', // 逾期未整改隐患
      name: 'overdueHiddenTotal',
      component: (resolve) => require(['@/components/safeCode/overdueHiddenTotal'], resolve)
    },
    {
      path: '/majorHiddenTotal', // 重大隐患数量
      name: 'majorHiddenTotal',
      component: (resolve) => require(['@/components/safeCode/majorHiddenTotal'], resolve)
    },
    {
      path: '/constructionProjects', // 在建项目
      name: 'constructionProjects',
      component: (resolve) => require(['@/components/safeCode/constructionProjects'], resolve)
    },
    {
      path: '/rectificationRate', // 整改率
      name: 'rectificationRate',
      component: (resolve) => require(['@/components/safeCode/rectificationRate'], resolve)
    },
    {
      path: '/nonWeeklyInspection', // 七天未周检项目
      name: 'nonWeeklyInspection',
      component: (resolve) => require(['@/components/safeCode/nonWeeklyInspection'], resolve)
    },
    {
      path: '/notAssessed', // 30天未考核
      name: 'notAssessed',
      component: (resolve) => require(['@/components/safeCode/notAssessed'], resolve)
    },
]