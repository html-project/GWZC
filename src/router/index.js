import Vue from 'vue'
import Router from 'vue-router'
import illegal from './illegal'  // 违章整改
import acceptance from './acceptance'  // 违章整改
import register from './register'  // 违章整改
import plan from './plan'
import notice from './notice'
import breakQuery from './breakQuery'  // 违章查询
import problemRectify from './problemRectify'  // 违章整改
import Weizregister from './Weizregister'
import hidden from './hidden' //隐患
import safe from './safe' //安全检查
import safeCode from './safeCode'
let router=[
    {
        path: '/', // 量化考核
        name: 'quantizationIndex',
        component: (resolve) => require(['@/components/quantization/quantizationIndex'], resolve),
        meta:{
            title:'量化考核'
        }
    },
   {
        path: '/quanPlan', // 量化考核计划
        name: 'quanPlan',
        component: (resolve) => require(['@/components/quantization/quanPlan'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
        path: '/quanRecord', // 量化考核记录
        name: 'quanRecord',
        component: (resolve) => require(['@/components/quantization/quanRecord'], resolve),
        meta:{
            title:'量化考核'
        }
    },
      {
        path: '/checkRecord', // 审核量化考核记录
        name: 'checkRecord',
        component: (resolve) => require(['@/components/quantization/checkRecord'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
        path: '/auditRecord', // 考核量化考核记录
        name: 'auditRecord',
        component: (resolve) => require(['@/components/quantization/auditRecord'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
        path: '/addRecord', // 新增量化考核记录
        name: 'addRecord',
        component: (resolve) => require(['@/components/quantization/addRecord'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
        path: '/recordDetails', // 量化考核记录详情
        name: 'recordDetails',
        component: (resolve) => require(['@/components/quantization/recordDetails'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
        path: '/addPlan', // 新增量化考核计划
        name: 'addPlan',
        component: (resolve) => require(['@/components/quantization/addPlan'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    // {
    //     path: '/selectBrank', // 选择违章现象
    //     name: 'selectBrank',
    //     component: (resolve) => require(['@/components/quantization/selectBrank'], resolve)
    // },
    // {
    //     path: '/selectCopyUser', // 选择抄送接收人
    //     name: 'selectCopyUser',
    //     component: (resolve) => require(['@/components/quantization/selectCopyUser'], resolve)
    // },
    // {
    //     path: '/selectBrank1', // 选择违章现象1
    //     name: 'selectBrank1',
    //     component: (resolve) => require(['@/components/quantization/selectBrank1'], resolve)
    // },
    {
        path: '/examinePlan', // 待审批考核计划
        name: 'examinePlan',
        component: (resolve) => require(['@/components/quantization/examinePlan'], resolve),
        meta:{
            title:'量化考核'
        }
    },
    {
         path: '/executeAssess', // 需要执行的量化考核
         name: 'executeAssess',
         component: (resolve) => require(['@/components/quantization/executeAssess'], resolve)
    },
    {
        path: '/planEnter', // 考核计划审批
        name: 'planEnter',
        component: (resolve) => require(['@/components/quantization/planEnter'], resolve)
    },
    /*{
        path: '/planDetails', // 考核计划详情
        name: 'planDetails',
        component: (resolve) => require(['@/components/quantization/planDetails'], resolve)
    },*/
    {
        path: '/registerBreak', // 登记违章
        name: 'registerBreak',
        component: (resolve) => require(['@/components/quantization/registerBreak'], resolve)
    },
    {
        path: '/brankDetails', // 违章详情
        name: 'brankDetails',
        component: (resolve) => require(['@/components/quantization/brankDetails'], resolve)
    },
    /*{
        path: '/brankAbarbeitung', // 违章整改通知书
        name: 'brankAbarbeitung',
        component: (resolve) => require(['@/components/quantization/brankAbarbeitung'], resolve)
    },*/
  /*  {
        path: '/changeAbarbeitung', // 整改通知书
        name: 'changeAbarbeitung',
        component: (resolve) => require(['@/components/quantization/changeAbarbeitung'], resolve)
    },*/
    {
        path: '/gapDetails', // 缺项详情
        name: 'gapDetails',
        component: (resolve) => require(['@/components/quantization/gapDetails'], resolve)
    },
    {
        path: '/issueDetails', // 问题项详情
        name: 'issueDetails',
        component: (resolve) => require(['@/components/quantization/issueDetails'], resolve)
    },
    {
        path: '/reasonableGap', // 合理缺项
        name: 'reasonableGap',
        component: (resolve) => require(['@/components/quantization/reasonableGap'], resolve)
    },
    {
        path: '/gapList', // 缺项问题列表
        name: 'gapList',
        component: (resolve) => require(['@/components/quantization/gapList'], resolve)
    },
    {
        path: '/problemItem', // 问题项
        name: 'problemItem',
        component: (resolve) => require(['@/components/quantization/problemItem'], resolve)
    },
    /*{
        path: '/breakNotice', // 违章积分通知书
        name: 'breakNotice',
        component: (resolve) => require(['@/components/quantization/breakNotice'], resolve)
    },*/
    {
        path: '/problemChildList', // 问题项子项列表
        name: 'problemChildList',
        component: (resolve) => require(['@/components/quantization/problemChildList'], resolve)
    },
    {
        path: '/haveRegBreak', // 已登记违章台账
        name: 'haveRegBreak',
        component: (resolve) => require(['@/components/quantization/haveRegBreak'], resolve)
    },
    {
        path: '/addBreakPerson', // 添加违章人员
        name: 'addBreakPerson',
        component: (resolve) => require(['@/components/quantization/addBreakPerson'], resolve)
    },
    {
        path: '/selectDeviceWork', // 选择分工
        name: 'selectDeviceWork',
        component: (resolve) => require(['@/components/quantization/selectDeviceWork'], resolve)
    },
    {
        path: '/riskManage2', // 风险管理
        name: 'riskManage2',
        component: (resolve) => require(['@/components/quantization/riskManage2'], resolve)
    },
    /*{
        path: '/safetyInspection', // 安全稽查列表
        name: 'safetyInspection',
        component: (resolve) => require(['@/components/quantization/safetyInspection'], resolve)
    },
    {
        path: '/securityCheck', // 安全检查列表
        name: 'securityCheck',
        component: (resolve) => require(['@/components/quantization/securityCheck'], resolve)
    },*/
    {
        path: '/registerBreakList',
        name: 'registerBreakList',
        component: (resolve) => require(['@/components/quantization/registerBreakList'],resolve)
    },
   /* {
        path: '/scoreNoticeDetails',
        name: 'scoreNoticeDetails',
        component: (resolve) => require(['@/components/quantization/scoreNoticeDetails'],resolve)
    },*/
]

let routes =new Set([...router,...illegal,...acceptance,...register,...plan,...notice,...breakQuery,...problemRectify,...Weizregister,...hidden,...safe,...safeCode])

let newRouter =new Router({
    routes
})
Vue.use(Router)
export default newRouter
