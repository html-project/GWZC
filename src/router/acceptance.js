export default[
    {
        path: '/acceptance', // 违章验收
        name: 'acceptance',
        component: (resolve) => require(['@/components/acceptance/acceptance'], resolve),
        meta:{
            title:'违章验收'
        }
    },
    {
        path: '/acceptanceDetails', // 违章验收--全部
        name: 'acceptanceDetails',
        component: (resolve) => require(['@/components/acceptance/acceptanceDetails'], resolve),
        meta:{
            title:'违章验收详情'
        }
    },
    {
        path: '/acceptanceDetails2', // 违章验收--我的
        name: 'acceptanceDetails2',
        component: (resolve) => require(['@/components/acceptance/acceptanceDetails2'], resolve),
        meta:{
            title:'违章验收详情'
        }
    },
]